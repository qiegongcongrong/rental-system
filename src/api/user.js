import request from '@/utils/request'

export const login = function (data) {
  return request.post('/user/login', data)
}

export const register = function (data) {
  return request.post('/user/registered', data)
}
