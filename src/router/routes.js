import Login from '@/views/login'
import Register from '@/views/register'
import Detail from '@/views/detail'
import Layout from '@/views/layout'
import Info from '@/views/secondes/info'
import House from '@/views/secondes/house'
import Collect from '@/views/secondes/collect'
import User from '@/views/secondes/user'

const routes = [
  { path: '/login', component: Login },
  { path: '/register', component: Register },
  { path: '/detail', component: Detail },
  {
    path: '/',
    component: Layout,
    redirect: '/house',
    children: [
      { path: '/house', component: House },
      { path: '/info', component: Info },
      { path: '/collect', component: Collect },
      { path: '/user', component: User }
    ]
  }
]
export default routes
