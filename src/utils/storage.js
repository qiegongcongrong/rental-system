const KEY = 'my-token-vant-mobile'

export const getToken = function () {
  return localStorage.getItem(KEY)
}

export const setToken = function (data) {
  localStorage.setItem(KEY, data)
}

export const removeToken = function () {
  localStorage.removeItem(KEY)
}
