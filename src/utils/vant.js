import Vue from 'vue'
import { Button, Tabbar, TabbarItem, NavBar, Form, Field, Toast } from 'vant'

Vue.use(Toast)
Vue.use(Form)
Vue.use(Field)
Vue.use(NavBar)
Vue.use(TabbarItem)
Vue.use(Tabbar)
Vue.use(Button)
